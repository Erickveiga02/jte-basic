String container(String image, String name) {
    return """
    - name: ${name}
      image: ${image}
      imagePullSecrets:
      - name: exampleK8sSecret
      imagePullPolicy: Always
      resources:
      requests:
          cpu: "100m"
          memory: 1Gi
      limits:
      memory: 1Gi
      command: ["tail", "-f", "/dev/null"]
"""
}

String call(libraries){
    // The top level keys are names of the libraries. ie: java8, java11, sonarqube, jira
    // This mapping need to exist for all the libraries. Should be loaded in as a resource
    library_to_pod_spec_mappings = [
                        "java8": ["image": 'maven:3.8.1-openjdk-8', "containerName": "java"],
                        "java11": ["image": 'maven:3.8.1-openjdk-11', "containerName": "java"],
                        "sonarqube": ["image": 'ubuntu', "containerName": "sonarqube"],
                        "jira": ["image": 'ubuntu', "containerName": "issueTracker"]
                        ]
    containers = ""
    libraries.each { library ->
        if (library_image_map[library.key]) {
            containers = containers + container(library_to_pod_spec_mappings[library.key]['image'], library_to_pod_spec_mappings[library.key]['containerName'])
        }
    }
    podTemplate = """
apiVersion: v1
kind: Pod
spec:
    containers:
    ${containers}
"""
    return podTemplate
}